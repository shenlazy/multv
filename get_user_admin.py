from tool.collection import Telnet_client
from tool.pyexcel import get_ip_add_info
from config import LOG_DIR
import paramiko
import re
import os

device_info = get_ip_add_info()
hostname_list = {}

patt_sw_dict = {
    "华为": r"<(.*)>",
    "中兴": r"(.*)#",
    "华三": r"<(.*)>",
}

patt_olt_dict = {
    "华为": r"(.*)>",
    "中兴": r"(.*)#",
    "贝尔": r"(.*)>#",
    "烽火": r"(.*)#"
}

def device_run(device_name, device_value):
    error = ''
    client = Telnet_client(device_name, "")
    try:
        if device_value['class'] == '烽火':
            buff = ''
            client.FH_login(device_value['username'], device_value['pwd'], device_value['fh_en_pwd'])
            buff += client.send_command(' ', [b'#'])
        else:
            buff = ''
            client.login(device_value['username'], device_value['pwd'])
            buff += client.send_command('display terminal user all', [b" }:"])
            buff += client.send_command('', [b">"])
        if "zhangzhiliang   Admin" in buff:
            error += f"{device_name}\t 具有该设备的部署权限\n"
            print("[+] 具有该设备的部署权限")
        else:
            error += f"{device_name}\t 没有该设备的部署权限\n"
            print("[-] 没有该设备的部署权限")
    except:
        print("[-] 无法连接")

    with open(os.path.join(LOG_DIR, 'is_admin.txt'), 'w+', encoding='utf-8', errors='ignore') as f:
        f.write(error)

    client.close()
    


def group_run(group_name, device_name_dict):
    # 同组中OLT设备和SW设备分开
    olt_dict = {k:v for k,v in device_name_dict.items() if v['type'] == 'olt'}

    for device_name, device_value in olt_dict.items():
        if device_value['is_bushu'] == "否":
            continue
        print(f"*****{device_name}*****")
        device_run(device_name, device_value)

def main():
    for group_name, device_name_dict in device_info.items():
        group_run(group_name, device_name_dict)

if __name__ == "__main__":
    main()