from tool.collection import SSH_client
from tool.pyexcel import get_excel_file
from tool.research import get_user_vlan
from config import PRESENT_DIR, FILE_DIR, SEMAPHORE

from eprogress import LineProgress, MultiProgressManager
from multiprocessing import Pool, freeze_support
import threading
import paramiko
import time
import os

start_time = time.time()

device_info, command_info,_ = get_excel_file()

#thread 参数
LOCK = threading.Lock()             # 互斥锁
progress_manager = MultiProgressManager() # 进度条(多线程会导致信息打印顺序错误)

def device_run(device_name, device_value, sw_name, sw, GROUP_DIR, vlan_all_list):
    SEMAPHORE.acquire()
    channel = SSH_client(sw['ip_add'], sw['username'], sw['pwd'])
    channel.telnet_connect(device_value['ip_add'], device_value['username'], device_value['pwd'], device_name)
    if device_value['class'] == "烽火":
        channel.fh_telnet_connect(device_value["fh_en_pwd"])

    try:
        device_log = ""
        command_list = command_info[device_value['class']][device_value['type']]

        #截止符的不同
        END_STR = f"{device_name}#"
        if device_value['class'] == "华为" or device_value['class'] == "中兴":
            END_STR = f"{device_name}#"
        elif device_value['class'] == "贝尔":
            END_STR = f"typ:{device_value['username']}>#"
        elif device_value['class'] == '烽火':
            END_STR = "Admin#"
        else:
            print(f"请确认{device_name}的设备类型是否填写正确")
            return

        #进度条
        progress_manager.put(device_value['ip_add'], LineProgress(total=100, title=device_name, width=25))
        count = 0

        for command in command_list:
            count += 1
            if device_value['class'] == '烽火' and command.startswith('cd'):
                path = "" if command.split()[1] == '.' else command.split()[1]
                END_STR = "Admin#" if path == "" else r"Admin\{}#".format(path)
            device_log += channel.send_command(f'{command}', END_STR)
            progress_manager.update(device_value['ip_add'], int((count/len(command_list))*100))
        with open(os.path.join(GROUP_DIR, f"olt_{device_name}.txt"), "w+", encoding="utf-8") as f:
            f.write(device_log)
    except Exception as e:
        print(e)
        print("命令列表里无命令")

    stack_vlan, _ = get_user_vlan(os.path.join(GROUP_DIR, f"olt_{device_name}.txt"), device_value['class'])
    
    LOCK.acquire()
    vlan_all_list.extend([x for x in stack_vlan if x not in vlan_all_list])
    LOCK.release()
    
    SEMAPHORE.release()
    #print("olt采集结束")
    channel.close()

def group_run(group_name, device_name_dict):
    '''
    group_name: 组的名字
    device_name_dict: 组里的信息
    '''
    #创建组的文件夹
    GROUP_DIR = os.path.join(FILE_DIR, group_name)
    if not os.path.exists(GROUP_DIR):
        os.makedirs(GROUP_DIR)
        
    vlan_all_list = []
    thread_list = []

    # 同组中OLT设备和SW设备分开
    olt_dict = {k:v for k,v in device_name_dict.items() if v['type'] == 'olt'}
    sw_dict = {k:v for k,v in device_name_dict.items() if v['type'] == 'sw'}
    for k,v in sw_dict.items():
        sw_name = k
        sw = v
    del sw_dict, k, v

    # 首先取出display vlan all命令的输出，和olt设备日志输出
    # 添加线程到线程列表中
    for device_name, device_value in olt_dict.items():
        #device_run(device_name, device_value, sw_name, sw, GROUP_DIR, vlan_all_list)
        t = threading.Thread(target=device_run, args=(device_name, device_value, sw_name, sw, GROUP_DIR, vlan_all_list))
        thread_list.append(t)

    for t in thread_list:
        t.start()
    for t in thread_list:
        t.join()
        
    VLAN_ALL_COMMAND = {
        "华三": "display mac-address dynamic vlan {}",
        "中兴": "show mac table dynamic vlan {}",
        "华为": "display mac-address dynamic vlan {}"
    }

    command_add = list(set(map(lambda x: VLAN_ALL_COMMAND[sw['class']].format(x), vlan_all_list)))
    #进度条
    progress_manager.put(sw['ip_add'], LineProgress(total=100, title=f"{group_name}:{sw_name}", width=50))
    count = 0

    # 采集交换机信息 连接到ssh
    channel = SSH_client(sw['ip_add'], sw['username'], sw['pwd'])
    # 遍历命令列表进行采集
    command_list = command_info[sw['class']][sw['type']] + command_add
    log_txt = ''
    for command in command_list:
        count += 1
        log_txt += channel.send_command(command, (f'{sw_name}>', f'{sw_name}#'))
        progress_manager.update(sw['ip_add'], int((count/len(command_list))*100))
    # 写入到文件中
    with open(os.path.join(GROUP_DIR, f"sw_{sw_name}.txt"), "a+", encoding="utf-8") as f:
        f.write(log_txt)
    # 关闭连接
    channel.close()
    print(f"[+] {sw_name}交换机采集完成")

def main_run(device_info):
    group_pool = Pool()

    for group_name, device_name_dict in device_info.items():
        #group_run(group_name, device_name_dict)
        group_pool.apply_async(group_run, args=(group_name, device_name_dict))

    group_pool.close()
    group_pool.join()
        
if __name__ == "__main__":
    main_run(device_info)

    print("共计用时:", time.time()-start_time)