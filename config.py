import os
import time
import threading


# 创建文件夹的常量
PRESENT_DIR = os.path.dirname(os.path.realpath(__file__))
PRESENT_TIME = time.strftime("%Y-%m-%d", time.localtime())

# 备份文件的文件夹
FILE_DIR = os.path.join(PRESENT_DIR, "Back_up", PRESENT_TIME)
if not os.path.exists(PRESENT_DIR):
    os.makedirs(PRESENT_DIR)
if not os.path.exists(FILE_DIR):
    os.makedirs(FILE_DIR)

# 更新的脚本文件夹
SCRIPT_DIR = os.path.join(PRESENT_DIR, "Script")
if not os.path.exists(SCRIPT_DIR):
    os.makedirs(SCRIPT_DIR)

# 下发配置的日志文件夹
DEPLOY_DIR = os.path.join(PRESENT_DIR, "Deploy")
if not os.path.exists(DEPLOY_DIR):
    os.makedirs(DEPLOY_DIR)

BEFORE_DIR = os.path.join(DEPLOY_DIR, 'Before')
if not os.path.exists(BEFORE_DIR):
    os.makedirs(BEFORE_DIR)

ING_DIR = os.path.join(DEPLOY_DIR, 'Ing')
if not os.path.exists(ING_DIR):
    os.makedirs(ING_DIR)

AFTER_DIR = os.path.join(DEPLOY_DIR, 'After')
if not os.path.exists(AFTER_DIR):
    os.makedirs(AFTER_DIR)

# log文件
LOG_DIR = os.path.join(PRESENT_DIR, 'Log')
if not os.path.exists(LOG_DIR):
    os.makedirs(LOG_DIR)


# pyexcel  调研表的名字, 设备的名字
DIAOYAN_EXCEL_FILE = "浙江移动组播实施调研内容表1.xls"
DEVICE_AND_COMMAND_EXCEL_FILE = "设备以及命令清单1.xls"

# get_read 线程数
# 目前中兴经过测试只能设置线程数为3，否则会大批量报错
# 华为支持10线程
SEMAPHORE = threading.Semaphore(10)
LOG_LOCK = threading.Lock()

# file_update  脚本模板地址
HW_FILE_PATH = "Script_demo/HW_demo.txt"
ZTE_FILE_PATH = "Script_demo/ZTE_demo.txt"
BE_FILE_PATH = "Script_demo/BE_demo.txt"
FH_FILE_PATH = "Script_demo/FH_demo.txt"
