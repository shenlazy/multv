from tool.collection import Telnet_client
from tool.pyexcel import get_excel_file, get_update_date
from tool.research import get_user_vlan
from config import PRESENT_DIR, FILE_DIR, SEMAPHORE, LOG_DIR, AFTER_DIR

from eprogress import LineProgress, MultiProgressManager
from multiprocessing import Pool, freeze_support
import threading
import paramiko
import time
import os
import re 

device_info, command_info,_ = get_excel_file()
info_dict = get_update_date()

#thread 参数
LOCK = threading.Lock()             # 互斥锁
progress_manager = MultiProgressManager() # 进度条(多线程会导致信息打印顺序错误)

def device_run(device_ip_add, device_value, GROUP_DIR):
    SEMAPHORE.acquire()

    tn = Telnet_client(device_ip_add, device_value['hostname'])
    
    if device_value['class'] == "烽火":
        tn.FH_login(device_value['username'], device_value['pwd'], device_value['fh_en_pwd'])
    else:
        tn.login(device_value['username'], device_value['pwd'])

    try:
        device_log = ""
        error = ''
        #command_list = command_info[device_value['class']]['after']
        command_dict = {
            "华为": ['en', 'config'],
            "烽火": [],
        }

        #截止符的不同
        END_STR = f"{device_value['hostname']}#"
        if device_value['class'] == "华为" or device_value['class'] == "中兴":
            END_STR = f"{device_value['hostname']}#"
        elif device_value['class'] == "贝尔":
            END_STR = f"typ:{device_value['username']}>#"
        elif device_value['class'] == '烽火':
            END_STR = "Admin#"
        else:
            print(f"请确认{device_value['hostname']}的设备类型是否填写正确")
            return
        config_end = f"{device_value['hostname']}(config)#"

        #进度条
        progress_manager.put(device_ip_add, LineProgress(total=100, title=f'{device_value["hostname"]}({device_ip_add})', width=25))

        for command in command_dict[device_value['class']]:
            if device_value['class'] == '烽火' and command.startswith('cd'):
                path = "" if command.split()[1] == '.' else command.split()[1]
                END_STR = "Admin#" if path == "" else r"Admin\{}#".format(path)          
            log = tn.send_command(command, [END_STR.encode(), config_end.encode()])
            device_log += log
        
        ctime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        if device_value['class'] == '华为':    
            log = tn.send_command('display mac-address vlan 39\n', [config_end.encode()])
            device_log += log
            #print(log)
            if "Failure" in log:
                error += f"{ctime}\t{device_ip_add}\t 没有vlan39的信息\n"
            elif info_dict[device_ip_add]['up_wuli'] not in log:
                error += f"{ctime}\t{device_ip_add}\t 没有上连口{info_dict[device_ip_add]['up_wuli']}的信息\n"
            progress_manager.update(device_ip_add, 50)
            
            up_wuli = info_dict[device_ip_add]['up_wuli'].replace(' /', '/')
            log = tn.send_command(f'display port vlan {up_wuli}\n', [config_end.encode()])
            device_log += log
            #print(log)
            if str(int(device_value['vlan'])) not in log:
                error += f"{ctime}\t{device_ip_add}\t 规划VLAN不存在\n"
            progress_manager.update(device_ip_add, 100)

            if error == '':
                error += f"{ctime}\t{device_ip_add}\t 部署验证通过\n"

        elif device_value['class'] == '烽火':
            _ = tn.send_command('cd service', [b'#'])
            _ = tn.send_command('terminal length 0', [b'#'])
            _ = tn.send_command('cd ..', [b'#'])
            _ = tn.send_command('cd vlan', [b'#'])
            log = tn.send_command('show vlan_port 39', [b'#'])
            device_log += log
            if not re.search(r'-+\s+\d\s+-+\s+tag', log) and not re.search(r'1\s+-+\s+-+\s+tag', log):
                error += f"{ctime}\t{device_ip_add}\t 没有vlan39的信息\n"
            progress_manager.update(device_ip_add, 30)

            log = tn.send_command(f'show vlan_port {int(device_value["vlan"])}', [b'#'])
            device_log += log
            if not re.search(r'-+\s+\d\s+-+\s+tag', log) and not re.search(r'1\s+-+\s+-+\s+tag', log):
                error += f"{ctime}\t{device_ip_add}\t 没有vlan{int(device_value['vlan'])}的信息\n"
            progress_manager.update(device_ip_add, 60)

            _ = tn.send_command('cd ..', [b'#'])
            _ = tn.send_command('cd card', [b'#'])
            progress_manager.update(device_ip_add, 70)

            log = tn.send_command(f'show pon-mac mainboard by-vlan 39', [b'#'])
            device_log += log
            if not re.search(r'\w+:\w+:\w+:\w+:\w+:\w+', log):
                error += f"{ctime}\t{device_ip_add}\t 没有vlan39的MAC信息\n"

            if error == '':
                error += f"{ctime}\t{device_ip_add}\t 部署验证通过\n"
            progress_manager.update(device_ip_add, 100)

        address = os.path.join(LOG_DIR, 'after_yanzheng.txt')
        LOCK.acquire()
        with open(address, "a+", encoding="utf-8") as f:
            f.write(error)
        LOCK.release()
        with open(os.path.join(AFTER_DIR, f'{device_ip_add}~{str(int(device_value["vlan"]))}.txt'), "w+", encoding="utf-8", errors='ignore') as f:
            f.write(device_log)        

    except Exception as e:
        print(e)
    
    SEMAPHORE.release()
    #print("olt采集结束")
    tn.close()

def group_run(group_name, device_name_dict):
    '''
    group_name: 组的名字
    device_name_dict: 组里的信息
    '''
    #创建组的文件夹
    GROUP_DIR = os.path.join(FILE_DIR, group_name)
    if not os.path.exists(GROUP_DIR):
        os.makedirs(GROUP_DIR)
        
    thread_list = []

    # 同组中OLT设备和SW设备分开
    olt_dict = {k:v for k,v in device_name_dict.items() if v['type'] == 'olt'}

    # 首先取出display vlan all命令的输出，和olt设备日志输出
    # 添加线程到线程列表中
    for device_name, device_value in olt_dict.items():
        if device_value['is_bushu'] == "否":
            continue
        #device_run(device_name, device_value, GROUP_DIR)
        t = threading.Thread(target=device_run, args=(device_name, device_value, GROUP_DIR))
        thread_list.append(t)

    for t in thread_list:
        t.start()
    for t in thread_list:
        t.join()

def main_run(device_info):

    for group_name, device_name_dict in device_info.items():
        group_run(group_name, device_name_dict)
        

if __name__ == "__main__":
    start_time = time.time()
    main_run(device_info)
    print("共计用时:", time.time()-start_time)
