# 中国移动萧山组播项目自动化部署

> version: v1.6.2
> auther: 沈柏均  
> update time: 2019.12.02

## 目的以及作用  

组播项目在地市测试完毕后进行全网部署，平均每天需要100台设备的割接，对于一些相同设备的，命令无差别的，可以进行自动化割接，已达到较高的工作效率。最终可以安全、快速的完成组播项目的部署

## 模块

* 采集筛选（筛选数据到excel中）
* 脚本更新（通过excel里的数据，来生成对应的脚本）
* 自动化部署
* 部署审核

## 模块详情
### 调研填表（华为、中兴、贝尔）
``` python get_write.py```

### 数据采集(华为、中兴、贝尔)
``` python get_read_telnet.py```

### 更新脚本(华为、中兴、贝尔、烽火)
``` python update.py```

### 获取hostname(华为、中兴、贝尔、烽火)
```python get_hostname_telnet.py```

### 下发配置（只支持华为）
``` python deploy ```

### 审核部署结果（暂时只支持华为和烽火）
``` python get_read_mac.py```

### 查验是否对设备有部署权限(支持是华为)
``` python get_user_admin.py```


