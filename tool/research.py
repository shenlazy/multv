"""
模块：实施调研表填写的各种方法

auther: 沈柏均
version: v0.12
"""
import re

# 设备名称
def get_device_name(path, device_type):
    '''
    text: 配置文件
    type: 设备类型

    name: 设备名称
    '''
    dir_name = path.split('\\')[-1]
    name_ip = dir_name.replace(".txt", "")
    if device_type == 'olt':
        name = name_ip.split('~')[1]
    else:
        name = name_ip.replace("sw_", "")

    return name

# OLT的设备型号 OLT的软件版本
def get_olt_info(olt_path, device_class):   
    '''
    olt_path: olt的地址
    device_class: 设备类型

    olt_version: olt的软件版本
    olt_product: olt的设备型号
    '''
    patt_version_str = {
        "华为": r'VERSION\s+:\s+(\w+)',
        "中兴": r'System\s+Description:\s+.*\s+Version(.*),\s+Copyright',
        "贝尔": r'\d +([\w.]+) +enabled',
        "烽火": r'system\sdevice\sversion\sis:(\w+)'
    }

    patt_product_str = {
        "华为": r'PRODUCT\s+:\s+(\w+)',
        "中兴": r'System\s+Description:\s+(.*)\s+Version'
    }

    with open(olt_path, 'r', encoding='utf-8', errors='ignore') as f:
        olt_text = f.read()

    try:
        version, product = "", ""
        version_pattern = re.compile(patt_version_str[device_class])
        version = version_pattern.search(olt_text).group(1)
        #  PRODUCT : MA5680T
        if device_class == "贝尔":
            product = "7360 ISAM FX-8"
        else:
            product_pattern = re.compile(patt_product_str[device_class])
            product = product_pattern.search(olt_text).group(1)
    except AttributeError as e:
        print("[-] 软件版本或型号未能匹配到，请查看备份脚本")
    except Exception as e:
        print(f"[-] 发生意外的错误:({type(e).__name__}){e}")

    print("[+] OLT的设备型号与软件版本采集完成")
    return version, product

# 用户VLAN的采集
def get_user_vlan(olt_path, device_class):
    '''
    olt_path: olt文件的地址
    device_class: 设备类型

    stack_vlan: 满足条件的用户双层vlan，做为之后的参数
    clash_vlan: 冲突域中的vlan，需要提取出来记录到excel表格中
    '''
    with open(olt_path, 'r', encoding='utf-8', errors='ignore') as f:
        olt_text = f.read()
    #all_vlan 所有的双层VLAN
    all_vlan = []

    #遍历正则匹配
    if device_class == "华为":
        pattern = re.compile(r'\s+(\d+)\s+smart\s+stacking.*')
        result = pattern.findall(olt_text)
    elif device_class == "中兴":
        pattern = re.compile(r'Details are following:\s+([\s\d,-]+)\s+')
        result_group = pattern.search(olt_text).group(1)
        result_text = result_group.replace('\n', '')
        pa = re.compile(r'(\w+-\w+)')
        res = pa.findall(result_text)
        for i in res:
            num = i.split('-')
            new_vlan = [str(x) for x in range(int(num[0]), int(num[1])+1)]
            result_text = result_text.replace(i, ','.join(new_vlan))
        result = result_text.split(',')
    elif device_class == "贝尔":
        pattern = re.compile(r'[\w/]+ +\w+ +[\w:]+\s+stacked:(\w+):.*')
        result_list = pattern.findall(olt_text)
        result = list(set(result_list))
    elif device_class == "烽火":
        pattern = re.compile(r'(\w+\s~\s\w+)')
        result_list = pattern.findall(olt_text)
        result = []
        for res in result_list:
            res = res.split(' ~ ')
            if res[0] == res[1]:
                result.append(res[0])
            else:
                result += [str(x) for x in range(int(res[0]), int(res[1])+1)]
    else:
        result = []      


    all_vlan += result

    # 筛选出满足条件的用户的stack_vlan
    # 冲突的vlan,需要记录到excel
    stack_vlan = list(filter(lambda x: 1024<= int(x) <2048, all_vlan))
    clash_vlan = list(filter(lambda x: 2176<= int(x) <2303, all_vlan))

    # 异常判断
    if stack_vlan == []:
        print("[-] 此olt脚本中没有满足条件的VLAN值，请手动确认")
 
    return stack_vlan, clash_vlan

# SW对应下联聚合口的采集
def sw_down_juhe(olt_path, sw_path, stack_vlan, olt_class, sw_class):
    '''
    olt_text: olt配置文件
    sw_text: sw配置文件
    stack_vlan: 满足条件的双层VLAN
    olt_class: olt设备类型
    sw_class: sw设备类型

    sw_down_juhe_list(str): SW下联聚合口的列表
    stack_vlan: 更新满足条件的VLAN
    '''
    # 满足条件的双层vlan对应的Mac列表
    stack_vlan_mac = []
    new_stack_vlan = []
    #ec82.6342.da21   1591  Dynamic   gpon-onu_1/2/2:3         vport 2  
    patt_olt_str = {
        "华为": r'.*gpon\s+(.*)\s+dynamic.*{}',
        "中兴": r'(.*)\s+{}\s+Dynamic\s+gpon-onu.*\s+vport',
        "贝尔": r'{} +((?:\w+:)+\w+)',
        "烽火": r'mac=([\w:]+)\svlan={}'
    }
    with open(olt_path, 'r', encoding='utf-8', errors='ignore') as f:
        olt_text = f.read()

    if olt_class == '烽火':
        try:
            pattern = re.compile(r'show fdb slot 1\n(.*?)#', flags=re.DOTALL)
            olt_text = pattern.search(olt_text).group(1)
        except Exception as e:
            print(f"[-] 发生意外的错误:({type(e).__name__}){e}")

    # 对应的Mac  正则匹配
    for vlan in stack_vlan:
        pattern = re.compile(patt_olt_str[olt_class].format(vlan))
        result = pattern.search(olt_text)
        try:
            if result:
                new_stack_vlan.append(vlan)
                mac = result.group(1)
                if olt_class == "贝尔" or olt_class == '烽火':
                    old_mac = mac.replace(':', '')
                    mac =f'{old_mac[:4]}-{old_mac[4:8]}-{old_mac[8:]}'
                stack_vlan_mac.append([vlan, mac.strip()])
            else:
                continue
        except AttributeError:
            print(f"[-] sw下连口采集：OLT信息中{vlan}对应MAC未匹配到")
            continue
        except Exception as e:
            print(f"[-] 发生意外的错误:({type(e).__name__}){e}")
            continue

    #聚合口列表
    sw_down_juhe_list = []

    patt_sw_str = {
        "华三": r'{}\s+{}\s+Learned\s+(\w*)\s+Y',
        "中兴": r'{}\s+{}\s+(\w+)\s+Dynamic',
        "华为": r'{}\s+{}\/-\s+([\w\S]+)\s+dynamic'
    }

    with open(sw_path, 'r', encoding='utf-8', errors='ignore') as f:
        sw_text = f.read()

    for mac_list in stack_vlan_mac:
        vlan, mac = mac_list
        if sw_class == "中兴":
            mac = mac.replace('-', '.')
        pattern = re.compile(patt_sw_str[sw_class].format(mac, vlan))
        try:
            result = pattern.search(sw_text).group(1)
            if result not in sw_down_juhe_list:
                sw_down_juhe_list.append(result)
        except AttributeError:
            print(f"[-] sw下连口采集：SW信息中没有{vlan}聚合口的信息")
        except Exception as e:
            print(f"[-] 发生意外的错误:({type(e).__name__}){e}")
    
    print("[+] SW对应下联聚合口采集完成")    
    return ",".join(sw_down_juhe_list), new_stack_vlan

# SW对应的上联聚合口
def sw_up_juhe(sw_path, stack_vlan, sw_down_juhe_list, sw_class):
    '''
    sw_path: sw的文件地址
    stack_vlan: 能找到Mac的双层用户VLAN
    sw_down_juhe_list: sw下联聚合口
    sw_class: 交换机的类型

    sw_up_juhe_list(str): SW对应上连聚合口
    bras_name: BRAS 名称
    bagg_mac: 交换机的各个地址
    '''

    #sw对应上连聚合口 BRAS名称
    sw_up_all_juhe_list = []
    sw_up_juhe_list = []
    bras_name = []
    bagg_mac = {}

    with open(sw_path, 'r', encoding='utf-8', errors='ignore') as f:
        sw_text = f.read()

    patt_up_juhe = {
        "华三": r'(\w+.\w+.\w+)\s+{}\s+Learned\s+(\w+)\s+Y',
        "中兴": r'(\w+.\w+.\w+)\s+{}\s+(\w+)\s+Dynamic',
        "华为": r'(\w+.\w+.\w+)\s+{}\/-\s+([\w\S]+)\s+dynamic'
    }

    patt_bagg = {
        "华三": r'{} +UP +[\w\S]+ +[\w\S]+ +\w+ +\w+ +(.*)',
        "中兴": r'{}(?:\s+\w+)+ +(.*(?:\n +.*)+)',
        "华为": r'{}\s+up\s+up\s+(.*)'
    }

    #找到上连聚合口
    for vlan in stack_vlan:
        # e868-1939-15c9   1055       Learned          BAGG18                   Y   
        pattern = re.compile(patt_up_juhe[sw_class].format(vlan))
        result = pattern.findall(sw_text)
        for mac, bagg in result:
            if bagg not in sw_down_juhe_list.split(",") and bagg not in sw_up_all_juhe_list:
                bagg_mac[bagg] = mac
                sw_up_all_juhe_list.append(bagg)

    #找bras名字
    for bagg in sw_up_all_juhe_list:
        pattern = re.compile(patt_bagg[sw_class].format(bagg))
        try:
            result = pattern.search(sw_text).group(1)
            result = result.replace(" ", "").replace("\n", "")
            if "BRAS" in result:
                if bagg in sw_up_juhe_list:
                    continue
                bras_name.append(f"{bagg}:{result}")
                sw_up_juhe_list.append(bagg)
            else:
                bagg_mac.pop(bagg)
        except AttributeError:
            print("[-] 无法找到此上联聚合口的信息")
            continue           
        except Exception as e:
            print(f"[-] 发生意外的错误:({type(e).__name__}){e}")
            continue

    print("[+] SW对应的上联聚合口采集完成")        
    return ','.join(sw_up_juhe_list), '\n'.join(bras_name), bagg_mac

# OLT 上连主物理口
def olt_up_wuli(olt_path, bagg_mac, device_class):
    '''
    olt_path: olt文件地址
    bagg_mac: 上联口各VLAN对应的mac地址
    device_class: 设备类型

    olt_up_wuli_master: olt上连主物理口
    '''
    olt_up_wuli_master_list = []

    patt_wuli_str = {
        "华为": r'.*eth.*{}\s+dynamic\s+(.*)\s+A.*',
        "中兴": r'{}\s+\w+\s+Dynamic\s+(.*)',
        "贝尔": r'{}\ssap:(.*?):\w+',
        "烽火": r'mac={}\s.*(trunk=\w+)'
    }

    with open(olt_path, 'r', encoding='utf-8', errors='ignore') as f:
        olt_text = f.read()

    for mac in bagg_mac.values():
        #        -     -  eth  04ec-bb23-2950 dynamic  0 /19/0 A -    -        1051
        if device_class == "中兴":
            mac = mac.replace('-', '.')
        elif device_class == "贝尔" or device_class == "烽火":
            old_mac = mac.replace('-', '')
            mac = ':'.join([old_mac[2*i:2*(i+1)] for i in range(len(old_mac)//2)])

        try:
            pattern = re.compile(patt_wuli_str[device_class].format(mac))
            result = pattern.search(olt_text)
            olt_up_wuli_master = result.group(1).strip()
            olt_up_wuli_master_list.append(olt_up_wuli_master)
        except AttributeError:
            print(f"[-] 未能匹配到{mac}所对应的上连物理口")
            continue
        except Exception as e:
            print(f"[-] 发生意外的错误:({type(e).__name__}){e}")

    olt_up_wuli_master = ';'.join(list(set(olt_up_wuli_master_list)))
    print("[+] OLT上连主物理口采集完成")  
    return olt_up_wuli_master

# OLT上连聚合口
def olt_up_juhe(olt_path, olt_up_wuli_master, device_class):
    '''
    olt_path: olt配置文件地址
    device_class: 设备类型

    olt_up_juhe_list(str): 上连聚合口
    '''

    with open(olt_path, 'r', encoding='utf-8', errors='ignore') as f:
        olt_text = f.read()

    #华为olt主端口
    if device_class == '华为':
        en = olt_up_wuli_master.replace(" ", "")
        pattern = re.compile(r'\s+link-aggregation add-member\s+{}\s+(.*)'.format(en))
        olt_up_juhe_list = pattern.findall(olt_text)
    elif device_class == "中兴":
        patt_str = r"interface\s+(gei_\d+/\d+/\d+)[.\s\S]*?{}"
        en = olt_up_wuli_master.replace("smartgroup", "smartgroup ")
        pattern = re.compile(patt_str.format(en + " mode active"), flags=re.DOTALL)
        olt_up_juhe_list = pattern.findall(olt_text)
    elif device_class == "贝尔":
        patt_str = r" +(.*?) +up +active"
        pattern = re.compile(patt_str)
        olt_up_juhe_list = pattern.findall(olt_text)
    elif device_class == "烽火":
        olt_up_juhe_list = ["暂无此信息"]

    print("[+] OLT上连聚合口采集完成")  
    return ";".join(olt_up_juhe_list)

# OLT 业务板卡统计（SLOT号）
def olt_yewu_banka(olt_path, olt_name, device_class):
    '''
    olt_path: 配置文件信息
    olt_name: olt的设备名称
    device_class: 设备的名称

    olt_yewu_banka_slot: OLT业务板卡统计（SLOT号）
    '''
    #板卡信息
    with open(olt_path, 'r', encoding='utf-8', errors='ignore') as f:
        olt_text = f.read()

    try:
        if device_class == "华为":
            return "设备类型无需此命令采集"
        elif device_class == "中兴":        
            pattern = re.compile(r'{}#show card\s+(.*?)\s+{}'.format(olt_name, olt_name), flags=re.DOTALL)
            banka_text = pattern.search(olt_text).group(1)
            banka_list = banka_text.split('\n')[2:]
            banka_tongji_list = []
            for banka in banka_list:
                banka = banka.split()[2:4]
                banka_tongji_list.append(':'.join(banka))
        elif device_class == "贝尔":
            pattern = re.compile(r'slot\s(lt.*)')
            banka_tongji_list = pattern.findall(olt_text)
        elif device_class == "烽火":
            pattern = re.compile(r'(\w+)\s+YES\s+(\w+)\s+')
            banka_tup = pattern.findall(olt_text)
            banka_tongji_list = []
            for banka in banka_tup:
            	banka_tongji_list.append(f"{banka[0]}:{banka[1]}")
        text = ';'.join(banka_tongji_list)
    except AttributeError as e:
        text = '无法匹配到OLT上的业务板卡信息'
        print("[-] 无法匹配到OLT上的业务板卡信息")
    except Exception as e:
        text = '发生意外错误'
        print(f"[-] 发生意外的错误:({type(e).__name__}){e}")

    print("[+] 业务板卡统计采集完成")      
    return text

# OLT上联板卡统计（SLOT号）
def olt_up_banka(olt_up_juhe_list, device_class):
    '''
    olt_up_juhe_list: 设备上联端口信息（OLT-SW-BRAS）

    olt_up_banka_slot: OLT上联板卡统计（SLOT号）
    '''
    # OLT上联板卡统计（SLOT号）
    olt_up_banka_slot = []

    try:
        if device_class == "华为":
            for slot in olt_up_juhe_list.split(";"):
                olt_up_banka_slot.append(slot.split()[0])
        elif device_class == "中兴":
            for slot in olt_up_juhe_list.split(";"):
                slot = slot.replace("gei_", "").split('/')[:2]
                olt_up_banka_slot.append('/'.join(slot))
        elif device_class == "贝尔":
            for slot in olt_up_juhe_list.split(";"):
                slot = slot.split(':')[0]
                olt_up_banka_slot.append(slot)
        elif device_class == "烽火":
            olt_up_banka_slot.append("暂无此信息")
    except Exception as e:
        print(f"[-] 发生意外错误:{e},具体参数olt_up_juhe_list:{olt_up_juhe_list}")
    print("[+] OLT上联板卡统计采集完成")  
    return ";".join(olt_up_banka_slot)


if __name__ == "__main__":
    olt = 'olt.txt'
    sw = 'sw.txt'
    olt_name = "olt"
    olt_class = "华为"
    sw_class = "中兴"

    stack_vlan, clash_vlan = get_user_vlan(olt, olt_class)
    sw_down_juhe_list, _ = sw_down_juhe(olt,sw,stack_vlan,olt_class,sw_class)
    #sw_up_juhe_list, bras_name, bagg_mac = sw_up_juhe(sw, stack_vlan, sw_down_juhe_list, sw_class)
    #olt_up_wuli_master = olt_up_wuli(olt, bagg_mac, olt_class)
    #olt_up_juhe_list = olt_up_juhe(olt, olt_up_wuli_master, olt_class)
    #olt_yewu_banka_info = olt_yewu_banka(olt, 'HY-HengShanTou-C300-1', olt_class)
    #olt_up_banka_info = olt_up_banka(olt_up_juhe_list, olt_class)
    print(f"1. {sw_down_juhe_list}\n2.{clash_vlan}")
