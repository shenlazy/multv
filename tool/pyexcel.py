"""
模块：项目中关于对excel的操作

auther: 沈柏均
version: v0.01
"""
from config import DIAOYAN_EXCEL_FILE, DEVICE_AND_COMMAND_EXCEL_FILE

import xlrd
from xlutils.copy import copy


# 提取需要采集的设备,以及命令
def get_excel_file():
    '''
    device_dict: 关于设备的清单
    command_dict: 关于命令的清单
    '''
    excel = xlrd.open_workbook(DEVICE_AND_COMMAND_EXCEL_FILE)

    device_table = excel.sheet_by_name("设备清单")
    command_table = excel.sheet_by_name("命令清单")

    # 处理设备的信息
    device_nrows = device_table.nrows
    device_dict = {}
    device_dishi_dict = {}
    for i in range(1, device_nrows):
        device_list = device_table.row(i)

        device_ip_add = device_list[1].value
        device_group = device_list[7].value

        device_name_dict = {
            "hostname": device_list[0].value,
            "username": device_list[2].value,
            "pwd": device_list[3].value,
            "fh_en_pwd": device_list[4].value,
            "class": device_list[5].value,
            "type": device_list[6].value,
            "vlan": device_list[10].value,
            "group": device_list[7].value,
            "is_bushu": device_list[11].value
        }

        device_dishi_dict[device_ip_add] = {
            "dishi": device_list[8].value,
            "quxian": device_list[9].value,
            'type': device_list[6].value,
            "class": device_list[5].value,
            'vlan': device_list[10].value,
            "ip_add": device_list[1].value,
            "group": device_list[7].value,
        }

        device_group_dict = {
            device_ip_add: device_name_dict
        }

        if device_group in device_dict:
            device_dict[device_group].update(device_group_dict)
        else:
            device_dict[device_group] = device_group_dict

    # 处理命令的信息
    command_nrows = command_table.nrows
    command_dict = {}
    for i in range(1, command_nrows):
        command_list = command_table.row(i)
        
        command_factory = command_list[2].value
        command_type = command_list[1].value
        command = command_list[0].value
        
        if command_factory in command_dict:
            if command_type in command_dict[command_factory]:
                command_dict[command_factory][command_type].append(command)
            else:
                command_dict[command_factory][command_type] = [command]
        else:
            command_dict[command_factory] = {command_type: [command]}
    
    return device_dict, command_dict, device_dishi_dict

def get_excel_list():
    excel = xlrd.open_workbook(DEVICE_AND_COMMAND_EXCEL_FILE)
    device_table = excel.sheet_by_name("设备清单") 

    device_nrows = device_table.nrows
    sw_dict = {}
    olt_list = []
    
    for i in range(1, device_nrows):
        list = device_table.row(i)
        group = list[7].value

        if list[6].value == 'olt':
            olt_list.append([list[0].value,list[7].value,list[1].value,list[11].value])
        elif list[6].value == 'sw':
            sw_dict[group] = [list[0].value]

    return sw_dict, olt_list


def get_deploy_info():
    excel = xlrd.open_workbook(DEVICE_AND_COMMAND_EXCEL_FILE)

    device_table = excel.sheet_by_name("设备清单")

    # 处理设备的信息
    device_nrows = device_table.nrows
    device_dict = {}
    sw_info = {}
    for i in range(1, device_nrows):
        device_list = device_table.row(i)

        device_ip_add = device_list[1].value

        device_name_info = {
            "hostname": device_list[0].value,
            "username": device_list[2].value,
            "pwd": device_list[3].value,
            "fh_en_pwd": device_list[4].value,
            "group": device_list[7].value,
            "vlan": device_list[10].value
        }

        if device_list[6].value == 'sw' and device_list[7].value not in sw_info:
            sw_info[device_list[7].value] = device_name_info
            
        if device_ip_add not in device_dict:
            device_dict[device_ip_add] = device_name_info
        else:
            print(device_ip_add)
            print(f"[-] 设备ip地址重复：{device_ip_add}")
        
    return device_dict, sw_info

# 将数据更新到excel表格中
def put_excel_file(olt_name, olt_version, olt_product,
                    clash_vlan, olt_up_banka_info, olt_yewu_banka_info,
                    olt_up_wuli_master,olt_up_juhe_list, sw_name, 
                    sw_down_juhe_list, sw_up_juhe_list, bras_name,
                    ip_add, vlan):
    *_ ,device_dishi_dict =  get_excel_file()
  
    oldwb = xlrd.open_workbook(DIAOYAN_EXCEL_FILE)
    oldws = oldwb.sheet_by_index(2)
    newwb = copy(oldwb)
    newws = newwb.get_sheet(2)

    # 获取当前的行数
    rows = oldws.nrows

    # 修改
    newws.write(rows, 0, device_dishi_dict[olt_name]["dishi"])              # olt所在地市
    newws.write(rows, 1, device_dishi_dict[olt_name]["quxian"])             # olt所在区县
    newws.write(rows, 2, olt_name)              # olt设备名称
    newws.write(rows, 4, olt_product)           # olt设备型号
    newws.write(rows, 5, olt_version)           # olt软件版本号
    newws.write(rows, 6, clash_vlan)            # 冲突vlan
    newws.write(rows, 7, olt_up_banka_info)     # OLT上联板卡统计（SLOT号）
    newws.write(rows, 8, olt_yewu_banka_info)   # OLT业务板卡统计（SLOT号）
    newws.write(rows, 9, olt_up_wuli_master)    # olt上连主物理口
    newws.write(rows, 10, olt_up_juhe_list)     # olt上连其他口
    newws.write(rows, 12, sw_name)              # sw设备名称
    newws.write(rows, 13, sw_down_juhe_list)    # sw下连聚合口
    newws.write(rows, 14, sw_up_juhe_list)      # sw上连聚合口
    newws.write(rows, 16, bras_name)            # bras 名称
    newws.write(rows, 18, vlan)                 # 规划VLAN
    newws.write(rows, 19, ip_add)               # ip地址
    newws.write(rows, 20, "是")                 # 是否更新出脚本

    #保存
    newwb.save(DIAOYAN_EXCEL_FILE)
    print("[+] 已写入excel表格中")

# 获取更新脚本所需要的数据
def get_update_date():
    wb = xlrd.open_workbook(DIAOYAN_EXCEL_FILE)
    ws = wb.sheet_by_name('OLT 采集信息说明')

    info_nrows = ws.nrows
    info_dict = {}

    for i in range(2, info_nrows):
        info_list = ws.row(i)
        info_ip_add = info_list[19].value
        
        if info_list[18].value == '':
            print(f"[-] 请输入{info_ip_add}的双层VLAN值")
            continue
        
        info_dict[info_ip_add] = {
            "up_banka": info_list[8].value,
            "up_wuli": info_list[9].value,
            "two_vlan": info_list[18].value,
            "hostname": info_list[2].value,
            "up_juhe": info_list[10].value,
            "is_gengxin": info_list[20].value,
        }

    return info_dict

# 获取hostname的格式
def get_ip_add_info():

    excel = xlrd.open_workbook(DEVICE_AND_COMMAND_EXCEL_FILE)

    device_table = excel.sheet_by_name("设备清单")

    device_dict = {}

    # 处理设备的信息
    device_nrows = device_table.nrows

    for i in range(1, device_nrows):
        device_group_dict = {}
        device_list = device_table.row(i)

        device_ip_add = device_list[1].value
        device_group = device_list[7].value

        device_ip_dict = {
            "username": device_list[2].value,
            "fh_en_pwd": device_list[4].value,
            "pwd": device_list[3].value,
            "class": device_list[5].value,
            "type": device_list[6].value,
            "is_bushu": device_list[11].value
        }

        device_group_dict[device_ip_add] = device_ip_dict

        if device_group in device_dict:
            device_dict[device_group].update(device_group_dict)
        else:
            device_dict[device_group] = device_group_dict

    return device_dict

def write_hostname_info(hostname_list):

    oldwb = xlrd.open_workbook(DEVICE_AND_COMMAND_EXCEL_FILE)
    oldws = oldwb.sheet_by_index(0)
    newwb = copy(oldwb)
    newws = newwb.get_sheet(0)

    # 获取当前的行数
    rows = oldws.nrows

    for i in range(1, rows):
        info_list = oldws.row(i)
        ip_add = info_list[1].value
        hostname = info_list[0].value

        newws.write(i, 0, hostname_list.get(ip_add, hostname))

    newwb.save(DEVICE_AND_COMMAND_EXCEL_FILE)
    print("[+] 已写入excel表格中")

if __name__ == "__main__":
    hostname_list = {
        '10.161.1.3': "hostname1",
        '10.205.52.35': "hostname2",
        '10.161.1.2': "hostname3",
        '10.161.0.149': "hostname4",
    }

    write_hostname_info(hostname_list)
