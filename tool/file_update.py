'''
模块：脚本更新模块

auther: 沈柏均
version: v0.01
'''
from config import PRESENT_DIR, HW_FILE_PATH, ZTE_FILE_PATH, BE_FILE_PATH, FH_FILE_PATH
import os

def HW_file_update(info):
    '''
    info: 需要替换的文本
    '''
    with open(os.path.join(PRESENT_DIR, HW_FILE_PATH), "r", encoding='utf-8') as f:
        text = f.read()

        up_wuil_list = info["up_wuli"].split('/')
        up_wuil_list[0] = up_wuil_list[0].rstrip()
    
        # two_vlan的内容
        vlan = int(info['two_vlan'])
        wuli_vlan = "{}/{} {}".format(up_wuil_list[0], up_wuil_list[1], up_wuil_list[2])
        
        new_text = text.format(vlan=vlan, wuli_vlan=wuli_vlan, up_zhu="/".join(up_wuil_list))
        return new_text

def ZTE_file_update(info):
    with open(os.path.join(PRESENT_DIR, ZTE_FILE_PATH), "r", encoding='utf-8') as f:
        text = f.read()

    new_text = text.format(up_wuli = info["up_wuli"], vlan = int(info["two_vlan"]))
    return new_text

def BE_file_update(info):
    with open(os.path.join(PRESENT_DIR, BE_FILE_PATH), "r", encoding='utf-8') as f:
        text = f.read()

    vlan = int(info['two_vlan'])

    vlan39_text = ''
    vlan_text = ''
    banka_list = info["up_banka"].split(";")
    for solt in banka_list:
        vlan39_text += f'configure service vpls 39 sap {solt} create\n'
        vlan_text += f'configure service vpls {vlan} sap {solt}:{vlan} create\n'

    up_wuli = info['up_wuli']
    new_text = text.format(up_wuli = up_wuli, vlan = vlan, vlan_text=vlan_text.rstrip('\n'), vlan39_text=vlan39_text.rstrip('\n'))
    return new_text

def FH_file_update(info):
    with open(os.path.join(PRESENT_DIR, FH_FILE_PATH), "r", encoding='utf-8', errors='ignore') as f:
        text = f.read()

    vlan = int(info['two_vlan'])
    up_zhu = info['up_wuli']
    up_juhe_old = info['up_juhe']

    up_juhe_old = up_juhe_old.split(":")
    up_juhe = f"slot {up_juhe_old[0]} port {up_juhe_old[1]}"
    
    new_text = text.format(vlan=vlan, up_zhu=up_zhu, up_juhe=up_juhe)
    return new_text

if __name__ == "__main__":
    pass
