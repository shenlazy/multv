"""
模块：登录设备采集命令

auther: 沈柏均
version: 0.01
"""
import paramiko
import telnetlib
import time

class SSH_client():
    '''
    ssh 客户端
    '''
    __slots__ = ('channel', 'client')

    def __init__(self, ip_add, username, pwd):
        while True:
            try:
                self.client = paramiko.SSHClient()
                self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                self.client.connect(ip_add, 22, username, pwd, timeout=15, allow_agent=False, look_for_keys=False)

                self.channel = self.client.invoke_shell()
                self.channel.settimeout(10)
                break
            except Exception as e:
                print(f"{e}:{type(e).__name__}")
                print('正在进行重新连接')
                self.client.close()

    def send_command(self, command, end_str):
        '''
        commend: 所需执行的命令（默认为一个回车）
        end_str: 这是结束符

        buff: 记录采集的数据
        '''
        buff = ''
        resp = ''
        self.channel.send(f'{command}\n')
        #print(r"发送命令{}".format(commend.strip()))
        while not buff.rstrip().endswith(end_str):
            try:
                resp = self.channel.recv(1024)
                if not resp:
                    break
                #print(resp.decode('utf-8'))
            except Exception as e:
                print(type(e).__name__, type(e).__name__ != 'time.out')
                #if type(e).__name__ != 'time.out':
                    #print(commend)
                    #print(resp)
                pass
            if type(resp) != str:
                resp = resp.decode('utf-8')
            buff += resp
        return buff

    def telnet_connect(self, ip_add, username, pwd, hostname):
        '''
        ip_add,username,pwd: 
        hostname: olt设备的名字,用户判断截止符
        '''
        # 发送telnet连接请求
        self.send_command(f'telnet {ip_add}', ("name:","ogin:"))
        #发送用户名
        self.send_command(username, "ssword:")
        #发送密码
        self.send_command(pwd, (f"{hostname}#", f"{hostname}>", f"typ:{username}>#", "User>"))
        print(f"[+] {hostname}登录成功")

    def fh_telnet_connect(self, fhpwd):
        #进入管理员模式
        self.send_command("en", "Password:")
        #输入密码
        self.send_command(fhpwd, "Admin#")
        print("[+] 进入烽火管理员模式")
    
    def close(self):
        self.client.close()


class Telnet_client():

    def __init__(self, ip_add, hostname):
        self.tn = telnetlib.Telnet()
        self.ip_add = ip_add
        self.hostname = hostname

    def send_command(self, command, end_str, time=None):
        self.tn.write(command.encode('ascii') + b'\n')
        *_, data = self.tn.expect(end_str, 3)
        return data.decode('utf-8', errors='ignore')

    def __open(self):
        count = 0
        while True:
            try:
                self.tn.open(self.ip_add, timeout=5)
                self.tn.expect([b'ame:'], 3)
                break
            except Exception:
                count += 1
                if count == 3:
                    print("[-] 无法登录，请人工查看")
                    break
                else:
                    print(f"[-] 正在尝试第{count}次重连")
                self.tn.close()


    def login(self, username, pwd):
        self.__open()
        _ = self.send_command(username, [b'ssword:'])
        _ = self.send_command(pwd, [f'{self.hostname}#'.encode('ascii'), 
                                    f'{self.hostname}>'.encode('ascii'), 
                                    f'typ:{username}>#'.encode('ascii'),
                                    b"User>"])

    def FH_login(self, username, pwd, fhpwd):
        self.login(username, pwd)
        _ = self.send_command('en', [b'User>'])
        _ = self.send_command(fhpwd, [b"Password:"])

    def close(self):
        self.tn.close()


class Telnet_client_test():

    def __init__(self, ip_add, hostname):
        self.tn = telnetlib.Telnet()
        self.ip_add = ip_add
        self.hostname = hostname

    def send_command(self, command, end_str, outtime=2):
        self.tn.write(command.encode('ascii') + b'\n')
        buff = ''
        while end_str not in buff:
            time.sleep(outtime)
            data = self.tn.read_very_eager()
            buff += data.decode('utf-8', errors='ignore')
        return buff
    
    def send_Login(self, command, outtime=1):
        self.tn.write(command.encode('ascii'))
        data = self.tn.read_very_eager()
        return data.decode('utf-8', errors='ignore')

    def send_command_Q(self, command, end_str):
        self.tn.write(command.encode('ascii') + b'\n')
        *_, data = self.tn.expect(end_str)
        return data.decode('utf-8', errors='ignore')

    def __open(self):
        count = 0
        while True:
            try:
                self.tn.open(self.ip_add)
                time.sleep(1)
                self.tn.read_very_eager()
                break
            except Exception:
                count += 1
                if count == 1:
                    print(f"{self.hostname} 设备连接不上，请确认")
                    break
                else:
                    print(f"[-] 正在尝试第{count}次重连")
                self.tn.close()


    def login(self, username, pwd):
        self.__open()
        _ = self.send_Login(username + '\n')
        _ = self.send_Login(pwd + '\n')

    def FH_login(self, username, pwd, fhpwd):
        self.login(username, pwd)
        _ = self.send_Login('en' + '\n')
        _ = self.send_Login(fhpwd + '\n')

    def close(self):
        self.tn.close()


if __name__ == "__main__":
    client = Telnet_client('10.161.131.90', 'LH-DaTian2-C300-3')
    client.login('zhangzhiliang', 'ABC@zubo123')

    text = client.send_command('en', [b'LH-DaTian2-C300-32#'])
    print(text)