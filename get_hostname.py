from tool.collection import SSH_client
from tool.pyexcel import get_ip_add_info, write_hostname_info
import paramiko
import re

device_info = get_ip_add_info()
hostname_list = {}

patt_sw_dict = {
    "华为": r"<(.*)>",
    "中兴": r"(.*)#",
    "华三": r"<(.*)>",
}

patt_olt_dict = {
    "华为": r"(.*)>",
    "中兴": r"(.*)#",
    "贝尔": r"(.*)>#",
    "烽火": r"(.*)#"
}

def device_run(device_name, device_value, sw_ip, sw):
    channel = SSH_client(sw_ip, sw['username'], sw['pwd'])    
    buff = channel.send_command('\n', '>')

    sw_hostname_patt = re.compile(patt_sw_dict[sw["class"]])
    sw_hostname = sw_hostname_patt.search(buff).group(1)
    if sw_ip not in hostname_list:
        hostname_list[sw_ip] = sw_hostname
    
    buff = ''
    channel.send_command(f'telnet {device_name}', ("name:","ogin:"))
    channel.send_command(device_value['username'], "ssword:")
    buff += channel.send_command(device_value['pwd'], (">", "#"))
    print("[+] 成功登录到olt")
    if device_value['class'] == '烽火':
        buff = ''
        channel.send_command('en', "ssword:")
        buff += channel.send_command(device_value["fh_en_pwd"], "#")

    olt_hostname_patt = re.compile(patt_olt_dict[device_value['class']])
    try:
        olt_hostname = olt_hostname_patt.search(buff).group(1)
    except AttributeError:
        olt_hostname = "无法连接"
    hostname_list[device_name] = olt_hostname

    channel.close()
    


def group_run(group_name, device_name_dict):
    # 同组中OLT设备和SW设备分开
    olt_dict = {k:v for k,v in device_name_dict.items() if v['type'] == 'olt'}
    sw_dict = {k:v for k,v in device_name_dict.items() if v['type'] == 'sw'}
    for k,v in sw_dict.items():
        sw_ip = k
        sw = v
    del sw_dict, k, v

    for device_name, device_value in olt_dict.items():
        print(f"*****{device_name}*****")
        device_run(device_name, device_value, sw_ip, sw)

def main():
    for group_name, device_name_dict in device_info.items():
        group_run(group_name, device_name_dict)

if __name__ == "__main__":
    main()
    write_hostname_info(hostname_list)