from tool.collection import Telnet_client_test
from tool.pyexcel import get_excel_file
from tool.research import get_user_vlan
from config import PRESENT_DIR, FILE_DIR, SEMAPHORE, BEFORE_DIR, AFTER_DIR, LOG_DIR

from eprogress import LineProgress, MultiProgressManager
from multiprocessing import Pool, freeze_support
import threading
import paramiko
import time
import os

device_info, command_info,_ = get_excel_file()
ctime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

#thread 参数
LOCK = threading.Lock()             # 互斥锁
LOG_LOCK = threading.Lock()
progress_manager = MultiProgressManager() # 进度条(多线程会导致信息打印顺序错误)

def device_run(device_name, device_value, GROUP_DIR):
    SEMAPHORE.acquire()

    tn = Telnet_client_test(device_value['ip_add'], device_name)
    
    if device_value['class'] == "烽火":
        tn.FH_login(device_value['username'], device_value['pwd'], device_value['fh_en_pwd'])
    else:
        tn.login(device_value['username'], device_value['pwd'])

    try:
        device_log = ""
        
        type = 'before'
        command_list = command_info[device_value['class']][type]

        #截止符的不同
        END_STR = f"{device_name}#"
        if device_value['class'] == "华为" or device_value['class'] == "中兴":
            END_STR = f"{device_name}#"
        elif device_value['class'] == "贝尔":
            END_STR = f"typ:{device_value['username']}>#"
        elif device_value['class'] == '烽火':
            END_STR = "Admin#"
        else:
            print(f"请确认{device_name}的设备类型是否填写正确")
            return

        #进度条
        progress_manager.put(device_value['ip_add'], LineProgress(total=100, title=f'{device_name}({device_value["ip_add"]})', width=25))
        count = 0

        for command in command_list:
            count += 1
            if device_value['class'] == '烽火' and command.startswith('cd'):
                path = "" if command.split()[1] == '.' else command.split()[1]
                END_STR = "Admin#" if path == "" else r"Admin\{}#".format(path)

            log = tn.send_command(command, END_STR, 2)
            if "Failure" in log:
                LOG_LOCK.acquire()
                with open(os.path.join(LOG_DIR, 'read_log.txt'), 'a+', encoding='utf-8') as f:
                    f.write(f"{ctime}\t{device_value['group']}\t{device_value['ip_add']}\t{device_name}:采集发生错误\n")
                LOG_LOCK.release()
                SEMAPHORE.release()
                tn.close()
                return
            device_log += log + '\n'
            progress_manager.update(device_value['ip_add'], int((count/len(command_list))*100))

    except Exception as e:
        LOG_LOCK.acquire()
        with open(os.path.join(LOG_DIR, 'read_log.txt'), 'a+', encoding='utf-8') as f:
            f.write(f"{ctime}\t{device_value['group']}\t{device_value['ip_add']}\t{device_name}:采集时发生错误{e}-{type(e).__name__}\n")
        LOG_LOCK.release()
        SEMAPHORE.release()
        tn.close()
        return 

    if device_log == '' or not device_log.rstrip().endswith(END_STR):
        LOG_LOCK.acquire()
        with open(os.path.join(LOG_DIR, 'read_log.txt'), 'a+', encoding='utf-8') as f:
            f.write(f"{ctime}\t{device_value['group']}\t{device_value['ip_add']}\t{device_name}:采集时发生错误\n") 
        LOG_LOCK.release()
        SEMAPHORE.release()
        tn.close()
        return 

    address = os.path.join(BEFORE_DIR, '{}-{}-{}.txt'.format(device_value['group'], device_name, device_value['ip_add']))
    with open(address, "w+", encoding="utf-8") as f:
        f.write(device_log)
    SEMAPHORE.release()
    #print("olt采集结束")
    tn.close()

def group_run(group_name, device_name_dict):
    '''
    group_name: 组的名字
    device_name_dict: 组里的信息
    '''
    #创建组的文件夹
    GROUP_DIR = os.path.join(FILE_DIR, group_name)
    if not os.path.exists(GROUP_DIR):
        os.makedirs(GROUP_DIR)
        
    thread_list = []

    # 同组中OLT设备和SW设备分开
    olt_dict = {k:v for k,v in device_name_dict.items() if v['type'] == 'olt'}

    # 首先取出display vlan all命令的输出，和olt设备日志输出
    # 添加线程到线程列表中
    for device_name, device_value in olt_dict.items():
        #device_run(device_name, device_value, GROUP_DIR)
        t = threading.Thread(target=device_run, args=(device_name, device_value, GROUP_DIR))
        thread_list.append(t)

    for t in thread_list:
        t.start()
    for t in thread_list:
        t.join()

def main_run(device_info):
    #group_pool = Pool()

    for group_name, device_name_dict in device_info.items():
        group_run(group_name, device_name_dict)
        #group_pool.apply_async(group_run, args=(group_name, device_name_dict))

    #group_pool.close()
    #group_pool.join()
        
if __name__ == "__main__":

    start_time = time.time()
    main_run(device_info)
    print("共计用时:", time.time()-start_time)
