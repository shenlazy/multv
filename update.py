from tool.pyexcel import get_excel_file, get_update_date
from tool.file_update import HW_file_update, ZTE_file_update, BE_file_update, FH_file_update
from config import SCRIPT_DIR

import os

*_, device_dishi_dict = get_excel_file()
info_dict = get_update_date()


for ip_add,info  in info_dict.items():
    if info['is_gengxin'] == '否':
        continue
    ip_add = ip_add
    name = info['hostname']
    vlan = info['two_vlan']
    group_name = device_dishi_dict[ip_add]['group']
    if device_dishi_dict[ip_add]['class'] == '华为':
        try:
            text = HW_file_update(info)
            with open(os.path.join(SCRIPT_DIR, f'{group_name}~{ip_add}~{name}~VLAN{vlan}.txt'), 'w+', encoding='utf-8') as f:
                f.write(text)
            print(f"[+] {ip_add}-{name}脚本生成")
        except IndexError:
            print(f"[-] {ip_add}-{name}可能信息不全，请重新查看调研表")
        except Exception as e:
            print(f"[-] {ip_add}-{name}发生未知错误，错误类型为{type(e).__name__}, 错误信息为:{e}")
    elif device_dishi_dict[ip_add]['class'] == '中兴':
        try:
            text = ZTE_file_update(info)
            with open(os.path.join(SCRIPT_DIR, f'{group_name}~{ip_add}~{name}~VLAN{vlan}.txt'), 'w+', encoding='utf-8') as f:
                f.write(text)
            print(f"[+] {ip_add}-{name}脚本生成")
        except IndexError:
            print(f"[-] {ip_add}-{name}可能信息不全，请重新查看调研表")
        except Exception as e:
            print(f"[-] {ip_add}-{name}发生未知错误，错误类型为{type(e).__name__}, 错误信息为:{e}")
    elif device_dishi_dict[ip_add]['class'] == '贝尔':
        try:
            text = BE_file_update(info)
            with open(os.path.join(SCRIPT_DIR, f'{group_name}-{ip_add}-{name}~VLAN{vlan}.txt'), 'w+', encoding='utf-8') as f:
                f.write(text)
            print(f"[+] {ip_add}-{name}脚本生成")
        except IndexError:
            print(f"[-] {ip_add}-{name}可能信息不全，请重新查看调研表")
        except Exception as e:
            print(f"[-] {ip_add}-{name}发生未知错误，错误类型为{type(e).__name__}, 错误信息为:{e}")
    elif device_dishi_dict[ip_add]['class'] == '烽火':
        try:
            text = FH_file_update(info)
            with open(os.path.join(SCRIPT_DIR, f'{group_name}~{ip_add}~{name}~VLAN{vlan}.txt'), 'w+', encoding='utf-8') as f:
                f.write(text)
            print(f"[+] {ip_add}-{name}脚本生成")
        except IndexError:
            print(f"[-] {ip_add}-{name}可能信息不全，请重新查看调研表")
        except Exception as e:
            print(f"[-] {ip_add}-{name}发生未知错误，错误类型为{type(e).__name__}, 错误信息为:{e}")