from config import SCRIPT_DIR, BEFORE_DIR, ING_DIR, AFTER_DIR, DEPLOY_DIR, LOG_DIR
from tool.pyexcel import get_deploy_info
from tool.collection import SSH_client, Telnet_client
from eprogress import CircleProgress, MultiProgressManager

import os
import threading
import re
import time

device_info, sw_info= get_deploy_info()
#thread 参数
SEMAPHORE = threading.Semaphore(5)
LOCK = threading.Lock()             # 互斥锁
progress_manager = MultiProgressManager() # 进度条(多线程会导致信息打印顺序错误)

def get_script_list():
    SCRIPT_FILE_LIST = os.listdir(SCRIPT_DIR)
    SCRIPT_FILE_LIST = list(filter(lambda x: '.txt' in x, SCRIPT_FILE_LIST))

    return SCRIPT_FILE_LIST

def HW_yanzheng(client, device_name, olt_info, ing_text):
    error = ''
    _ = client.send_command('scroll', [b' }:'])
    _ = client.send_command('', [b'\)#'])
    print('command: display vlan 39')
    _ = client.send_command('display vlan 39', [b'\]:', b'\)#', b' }:',
                                                        f'{olt_info["hostname"]}#'.encode('ascii')])
    log = client.send_command('', [b'\]:', b'\)#', b' }:',
                                                        f'{olt_info["hostname"]}#'.encode('ascii')])
    print(log)
    if "Failure" in log:
        error += f"{device_name}\tvlan 39 没有配置成功\n"
    else:
        ing_text += log

    print(f'command: display vlan {int(olt_info["vlan"])}')
    _ = client.send_command(f'display vlan {int(olt_info["vlan"])}', [b'\]:', b'\)#', b' }:',
                                                        f'{olt_info["hostname"]}#'.encode('ascii')])
    log = client.send_command('', [b'\]:', b'\)#', b' }:',f'{olt_info["hostname"]}#'.encode('ascii')])
    print(log)
    if "Failure" in log:
        error += f"{device_name}\tvlan{olt_info['vlan']}  没有配置成功\n"
    else:
        ing_text += log
    
    print('command: display current-configuration section btv')
    _ = client.send_command('display current-configuration section btv\n', [b'\]:', b'\)#', b' }:',
                                                        f'{olt_info["hostname"]}#'.encode('ascii')])
    log = client.send_command('', [b'\]:', b'\)#', b' }:',
                                                        f'{olt_info["hostname"]}#'.encode('ascii')])
    print(log)
    if "multicast-vlan 39" not in log:
        error += f"{device_name}\tmulticast-vlan 39没有配置成功\n"
    elif "igmp version v2" not in log:
        error += f"{device_name}\tigmp version v2没有配置成功\n"
    elif "igmp match mode disable" not in log:
        error += f"{device_name}\tigmp match mode disable没有配置成功\n"
    elif "igmp mode proxy" not in log:
        error += f"{device_name}\tigmp mode proxy没有配置成功\n"
    elif "igmp uplink-port" not in log:
        error += f"{device_name}\tigmp uplink-port没有配置成功\n"
    else:
        ing_text += log

    _ = client.send_command('save', [b' }:'])
    _ = client.send_command('', [b'\)#'])
    
    return ing_text, error

def ssh_run(script):
    SEMAPHORE.acquire()
    device_name = script.replace('.txt', '')
    olt_info = device_info[device_name]
    sw = sw_info[olt_info['group']]
    # 连接设备
    try:
        client = SSH_client(sw['ip_add'], sw['username'], sw['pwd'])
        client.telnet_connect(olt_info['ip_add'], olt_info['username'], olt_info['pwd'], device_name)

        script_file = os.path.join(SCRIPT_DIR, script)
        with open(script_file, 'r', encoding='utf-8', errors='ignore') as f:
            log_text = ''
            for line in f.readlines():
                #刷入命令
                progress_manager.update(olt_info['ip_add'], 1)
                log = client.send_command(line.rstrip('\n'), (':', '#', '>'))
                log_text += log.strip()
        client.close()
        progress_manager.update(olt_info['ip_add'], 0)
        print(f"[+] {device_name}部署完成")
        SEMAPHORE.release()
    except Exception as e:
        print(f'[-] 发生未知错误{e}:{type(e).__name__}')

    deploy_file = os.path.join(DEPLOY_DIR, script)
    with open(deploy_file, 'w+', encoding='utf-8', errors='ignore') as f:
        f.write(log_text)

def telnet_run(script):
    #SEMAPHORE.acquire()
    device_ip = script.replace('.txt', '')
    device_name = device_ip.split('~')[1]
    olt_info = device_info[device_name]
    print(f"****{device_name}****")
    # 连接设备
    
    client = Telnet_client(device_name, olt_info['hostname'])
    client.login(olt_info['username'], olt_info['pwd'])

    #进度条
    #progress_manager.put(olt_info['ip_add'], CircleProgress(title=device_name))

    script_file = os.path.join(SCRIPT_DIR, script)
    ing_text = ''
    with open(script_file, 'r', encoding='utf-8', errors='ignore') as f:
        for line in f.readlines():
            try:
                print(f"command: {line}")
                log = client.send_command(line.rstrip('\n'), [b'\]:', b'\)#', b'>', b' }:',
                                                            f'{olt_info["hostname"]}#'.encode('ascii')])
                print(f"{log}")
                ing_text += log.strip()
            except Exception as e:
                print(f'[-] 发生未知错误{e}:{type(e).__name__}')
        
        ing_text, error = HW_yanzheng(client, device_name, olt_info, ing_text)
    
        client.close()
        #progress_manager.update(olt_info['ip_add'], 0)
        print(f"[+] {device_name}部署完成")

    with open(os.path.join(ING_DIR, f"{device_name}.txt"), 'w+', encoding='utf-8', errors='ignore') as f:
        f.write(ing_text)
    if error != '':
        with open(os.path.join(LOG_DIR, "deploy_log.txt"), "w+", encoding='utf-8', errors='ignore') as f:
            f.write(error)
    else:
        os.remove(script_file)

def ssh_main():
    # 整理设备脚本列表
    script_list = get_script_list()
    thread_list = []
    # 逐个打开脚本
    for script in script_list:
        t = threading.Thread(target=ssh_run, args=(script, ))
        thread_list.append(t)

    for t in thread_list:
        t.start()
    for t in thread_list:
        t.join()

def telnet_main():
    # 整理设备脚本列表
    #thread_list = []
    script_list = get_script_list()
    # 逐个打开脚本
    for script in script_list:
        telnet_run(script)
        #t = threading.Thread(target=telnet_run, args=(script, ))
        #thread_list.append(t)

    #for t in thread_list:
        #t.start()
    #for t in thread_list:
        #t.join()

if __name__ == "__main__":
    telnet_main()
