from tool import research, pyexcel
from eprogress import LineProgress, MultiProgressManager
from config import FILE_DIR

import time
import os
import threading

_, _, info_class = pyexcel.get_excel_file()
LOCK = threading.Lock()
progress_manager = MultiProgressManager() # 进度条(多线程会导致信息打印顺序错误)

def main(olt, sw):

    # 设备名称采集
    olt_name = research.get_device_name(olt, "olt")
    sw_name = research.get_device_name(sw, "sw")
    #print("OLT:{}\nSW:{}".format(olt_name, sw_name))

    # olt 设备类型的采集
    olt_class = info_class[olt_name]['class']
    sw_class = info_class[sw_name]['class']
    ip_add = info_class[olt_name]['ip_add']
    vlan = info_class[olt_name]['vlan']
    if olt_class == "烽火":
        print(f"[-] 暂不支持{olt_class}")
        return

    progress_manager.put(ip_add, LineProgress(total=100, title=f'{olt_name}-{ip_add}', width=50))
    # OLT的信息采集
    olt_version, olt_product = research.get_olt_info(olt, olt_class)
    #print(f'version:{olt_version}\nproduct:{olt_product}')

    # 用户vlan的采集
    stack_vlan, clash_vlan = research.get_user_vlan(olt, olt_class)
    #print(f"双层vlan:{stack_vlan}\n冲突VLAN:{clash_vlan}")

    # SW对应的下连聚合口的采集
    sw_down_juhe_list, stack_vlan = research.sw_down_juhe(olt, sw, stack_vlan, olt_class, sw_class)
    #print(sw_down_juhe_list,'\n',stack_vlan)

    # SW对应的上连聚合口的采集
    sw_up_juhe_list, bras_name, bagg_mac = research.sw_up_juhe(sw, stack_vlan, sw_down_juhe_list, sw_class)
    #print(sw_up_juhe_list, '\n', bras_name)

    # OLT上连主物理口
    olt_up_wuli_master = research.olt_up_wuli(olt, bagg_mac, olt_class)
    #print(olt_up_wuli_master)

    # OLT 上联聚合口
    olt_up_juhe_list = research.olt_up_juhe(olt, olt_up_wuli_master, olt_class)

    # OLT业务板卡统计（SLOT号）
    olt_yewu_banka_info = research.olt_yewu_banka(olt, olt_name, olt_class)

    # OLT上联板卡统计（SLOT号）需要在SW对应的上连聚合口的采集方法后
    olt_up_banka_info = research.olt_up_banka(olt_up_juhe_list, olt_class)

    # 更新到excel中
    pyexcel.put_excel_file(olt_name, olt_version, olt_product,
                            clash_vlan, olt_up_banka_info, olt_yewu_banka_info,
                            olt_up_wuli_master, olt_up_juhe_list, sw_name, 
                            sw_down_juhe_list, sw_up_juhe_list, bras_name,
                            ip_add, vlan)


if __name__ == "__main__":

    group_dict, olt_list = pyexcel.get_excel_list()

    #print(group_dict)
    
    for olt in olt_list:
        if olt[3] == '否':
            continue
        print(f'****{olt[0]}****')
        sw = group_dict[olt[1]]
        olt_path = os.path.join(FILE_DIR, olt[1], f'olt~{olt[0]}~{olt[2]}.txt')
        sw_path = os.path.join(FILE_DIR, olt[1], f'sw_{sw[0]}.txt')

        main(olt_path, sw_path)