from tool.collection import Telnet_client
from tool.pyexcel import get_ip_add_info, write_hostname_info
import paramiko
import re

device_info = get_ip_add_info()
hostname_list = {}

patt_sw_dict = {
    "华为": r"<(.*)>",
    "中兴": r"(.*)#",
    "华三": r"<(.*)>",
}

patt_olt_dict = {
    "华为": r"(.*)>",
    "中兴": r"(.*)#",
    "贝尔": r"(.*)>#",
    "烽火": r"(.*)#"
}

def device_run(device_name, device_value):
    client = Telnet_client(device_name, "")
    try:
        if device_value['class'] == '烽火':
            buff = ''
            client.FH_login(device_value['username'], device_value['pwd'], device_value['fh_en_pwd'])
            buff += client.send_command(' ', [b'#'])
        else:
            buff = ''
            client.login(device_value['username'], device_value['pwd'])
            buff += client.send_command(' ', [b'#', b'>'])
        print("[+] 成功登录到olt")
    except:
        print("[-] 无法连接")

    olt_hostname_patt = re.compile(patt_olt_dict[device_value['class']])
    try:
        olt_hostname = olt_hostname_patt.search(buff).group(1)
    except AttributeError:
        olt_hostname = "无法连接"
    hostname_list[device_name] = olt_hostname

    client.close()
    


def group_run(group_name, device_name_dict):

    olt_dict = {k:v for k,v in device_name_dict.items() if v['type'] == 'olt'}

    for device_name, device_value in olt_dict.items():
        if device_value['is_bushu'] == "否":
            continue
        print(f"*****{device_name}*****")
        device_run(device_name, device_value)

def main():
    for group_name, device_name_dict in device_info.items():
        group_run(group_name, device_name_dict)

if __name__ == "__main__":
    main()
    write_hostname_info(hostname_list)